class EmployeeEXP {
  #age;
  #salary;
  #name;
  constructor(name, age, salary) {
    this.#age = age;
    this.#salary = salary;
    this.#name = name;
  }

  getName() {
    return this.#name;
  }
  getAge() {
    return this.#age;
  }
  getSalary() {
    return this.#salary;
  }

  setName(name) {
    this.#name = name;
  }
  setAge(age) {
    this.#age = age;
  }
  setSalary(salary) {
    this.#salary = salary;
  }
}

console.log('***************************************');
console.log('********* script_experimental *********');
console.log('***************************************');

const employeeEXP = new EmployeeEXP('Experimental', 33, 3300);
console.log(employeeEXP);
console.log(employeeEXP.getAge());
employeeEXP.setAge(43);
console.log(employeeEXP.getAge());
console.log(employeeEXP.name);
