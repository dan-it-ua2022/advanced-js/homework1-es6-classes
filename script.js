class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  getName() {
    return this._name;
  }
  getAge() {
    return this._age;
  }
  getSalary() {
    return this._salary;
  }

  setName(name) {
    this._name = name;
  }
  setAge(age) {
    this._age = age;
  }
  setSalary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  getSalary() {
    return this._salary * 3;
  }
  getLang() {
    return this._lang;
  }

  setLang(lang) {
    return (this._lang = lang);
  }
}

const employee = new Employee('dima', 23, 5000);
console.log(employee);
console.log(employee.getAge());
employee.setAge(43);
console.log(employee.getAge());
console.log(employee._name);

const programmer = new Programmer('jek', 34, 3000, 'c++');
console.log(programmer);
console.log(programmer.getAge());
console.log(programmer.getLang());
console.log(programmer.getSalary());

const programmer2 = new Programmer('lif', 22, 1000, 'js');
console.log(programmer2);
